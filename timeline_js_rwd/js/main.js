var bazaDanych = [
  ['02.06.2015','LOREM IPSUM DOLOR SIT','css/assets/icons/favorite-heart-button.png'],
  ['11.06.2015','LOREM IPSUM DOLOR SIT','css/assets/icons/flask-outline.png'],
  ['15.06.2015','LOREM IPSUM DOLOR SIT','css/assets/icons/legal-hammer.png'],
  ['22.06.2015','LOREM IPSUM DOLOR SIT','css/assets/icons/students-cap.png'],
  ['30.06.2015','LOREM IPSUM DOLOR SIT','css/assets/icons/trophy.png'],
];
//Wybór daty
var data = 10;

createUlElement(data);

function createSimpleElement(element){
    var tag, zrodlo;
    if(element.tag){
        tag = document.createElement(element.tag);

          if(element.classes && Array.isArray(element.classes)){
              for(i=0;i<element.classes.length;i++){
              tag.classList.add(element.classes[i]);
            }
          }
          if(element.tag === "IMG"){
            tag.src = element.src;
            tag.title = element.title;
          }

          if(element.textNode){
            tag.appendChild(document.createTextNode(element.textNode));
          }

          if(element.id){
            tag.id = element.id;
          }
    }
    return tag;
  }

function createUlElement(dzisiejszaData){

      var lista = createSimpleElement({
        "tag": "UL",
        "classes": ["lista"]
      });

      for(var i=0 ; i<bazaDanych.length;i++){
        if(bazaDanych[i] && Array.isArray(bazaDanych[i])){

          var pozycja = createSimpleElement({
            "tag": "LI",
            "classes": ["pozycja"]
          })


          // Rozłożenie wydarzeń na osi czasu
          var dzien = bazaDanych[i][0].slice(0,2);
          var progres = ((99/30) * dzien) + '% ';
          pozycja.style.left =  progres;

          // Obliczenie paska progresu na podstawie założonej dzisiejszej daty
          var stanProgresu = ((dzisiejszaData * 100)/30) + '%';
          var pasek = document.getElementById('progressBar');
          pasek.style.width = stanProgresu;


          var ikonaBack = createSimpleElement({
            "tag": "DIV",
            "classes": ["ikonaTlo"]
          })

          var ikona = createSimpleElement({
            "tag": "IMG",
            "classes": ["ikona"],
            "title": bazaDanych[i][1],
            "src": bazaDanych[i][2]
          })

          //Kolorowanie ikon
          if(dzien <= dzisiejszaData){
            ikonaBack.style.background= "#99E2FA";
            ikona.style.filter="invert(0%)";
          }

          //Tworzy tooltip
          var tooltip = createSimpleElement({
            "tag": "DIV",
            "classes": ["tooltip"],
            // "textNode": bazaDanych[i][0] + " " + bazaDanych[i][1]
          })
                // Zawartość tooltip'a
                var tooltipData = createSimpleElement({
                  "tag": "DIV",
                  "classes": ["tooltipData"],
                  "textNode": bazaDanych[i][0]
                })
                var tooltipText = createSimpleElement({
                  "tag": "DIV",
                  "classes": ["tooltipText"],
                  "textNode": bazaDanych[i][1]
                })

          pozycja.appendChild(ikonaBack);
          ikonaBack.appendChild(ikona);

          tooltipData.appendChild(tooltipText);
          tooltip.appendChild(tooltipData);
          pozycja.appendChild(tooltip);
          lista.appendChild(pozycja);
        }
      }

      var dokument =  document.getElementById('timeLineContent');
      dokument.appendChild(lista);
      // document.body.appendChild(sekcja);
    }
